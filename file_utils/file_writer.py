# -*- coding: utf-8 -*-
'''
Created on 2016年12月19日

@author: ZhuJiahui
'''

from openpyxl import Workbook

def write_1d_to_text(write_filename, data_list, encode_type='utf-8'):
    '''
    将一维字符串list写入文本文件
    
    :param write_filename: 文件名
    :param data_list: 字符串list
    '''
    
    with open(write_filename, 'w', encoding=encode_type) as f:
        for i in range(len(data_list) - 1):
            f.write(str(data_list[i]))
            f.write('\n')
        
        f.write(str(data_list[len(data_list) - 1]))

def quick_write_1d_to_text(write_filename, data_list, encode_type='utf-8'):
    '''
    将一维字符串list写入文本文件，一次性写入全部
    
    :param write_filename: 文件名
    :param data_list: 字符串list
    '''
    
    with open(write_filename, 'w', encoding=encode_type) as f:
        f.write('\n'.join(data_list))


def write_to_excel(write_filename, data_list):
    '''
    将二维数值list写入Excel
    
    :param write_filename: 文件名
    :param data_list: 2维数据
    '''
    work_book = Workbook()
    this_sheet = work_book.create_sheet(0)
    for i in range(len(data_list)):
        this_sheet.append(data_list[i].tolist())
    
    work_book.save(write_filename)
    

if __name__ == '__main__':
    pass