# -*- coding: utf-8 -*-
'''
Created on 2016年12月19日

@author: ZhuJiahui
'''


import os
import numpy as np
from openpyxl import load_workbook
from file_utils.file_writer import write_to_excel


def read_to_1d_list(read_filename):
    '''
    读取文本文件为1维字符串list
    :param read_filename: 文件名
    '''

    result_list = []

    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            result_list.append(each_line.strip())

    return result_list

def read_to_1d_list_gbk(read_filename):
    '''
    读取文本文件为1维字符串list 采用GBK编码
    :param read_filename: 文件名
    '''

    result_list = []

    with open(read_filename, 'r', encoding='gbk') as f:
        for each_line in f:
            result_list.append(each_line.strip())

    return result_list

def read_selected_column(read_filename, delimiter, column_index):
    '''
    读取多列文本文件中的某一列为1维字符串list

    :param read_filename: 文件名
    :param delimiter: 列分隔符
    :param column_index: 指定列索引
    '''

    result_list = []

    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            result_list.append(each_line.strip().split(delimiter)[column_index])

    return result_list


def read_to_2d_list(read_filename, delimiter):
    '''
    读取文本文件为2维字符串list

    :param read_filename: 文件名
    :param delimiter: 列分隔符
    '''
    
    result_list = []
    
    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            result_list.append(each_line.strip().split(delimiter))
        
    return result_list


def read_to_2d_list_gbk(read_filename, delimiter):
    '''
    读取文本文件为2维字符串list

    :param read_filename: 文件名
    :param delimiter: 列分隔符
    '''
    
    result_list = []
    
    with open(read_filename, 'r', encoding='gbk') as f:
        for each_line in f:
            result_list.append(each_line.strip().split(delimiter))
        
    return result_list


def read_to_2d_list_s(read_filename, delimiter, start_column_index):
    '''
    读取文本文件为2维字符串list, 提供每一行的开始列的索引
    
    :param read_filename: 文件名
    :param delimiter: 列分隔符
    :param start_column_index: 开始列的索引
    '''
    
    result_list = []
    
    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            result_list.append(each_line.strip().split(delimiter)[start_column_index:])
        
    return result_list


def read_to_2d_list_se(read_filename, delimiter, start_column_index, end_column_index):
    '''
    读取文本文件为2维字符串list, 提供每一行的开始列和结束列的索引
    
    :param read_filename: 文件名
    :param delimiter: 列分隔符
    :param start_column_index: 开始列的索引
    :param end_column_index: 结束列的索引
    '''
    
    result_list = []
    
    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            result_list.append(each_line.strip().split(delimiter)[start_column_index:end_column_index])
        
    return result_list


def read_to_2_list(read_filename, delimiter):
    '''
    读取文本文件的前两列为字符串list
    :param read_filename: 文件名
    :param delimiter: 列分隔符
    '''
    
    result_list1 = []
    result_list2 = []
    with open(read_filename, 'r', encoding='utf-8') as f:
        for each_line in f:
            line_segment = each_line.strip().split(delimiter)
            assert(len(line_segment) >= 2)
            result_list1.append(line_segment[0])
            result_list2.append(line_segment[1])
        
    return result_list1, result_list2
    

def read_excel_to_2d_list(read_filename, start_row_index, start_column_index):
    '''
    读取Excel文件为2维字符串list, 提供开始位置的行和列的索引(从0开始)
    注意：openpyxl中的Excel的单元格下标是从1开始的
    
    :param read_filename: 文件名
    :param start_row_index: 开始的行的索引
    :param start_column_index: 开始的列的索引
    '''
    
    work_book = load_workbook(read_filename)
    sheet_names = work_book.get_sheet_names()
    this_sheet = work_book.get_sheet_by_name(sheet_names[0])
    
    result_list = []
    
    for i in range(start_row_index + 1, this_sheet.max_row + 1):
        temp_list = []
        for j in range(start_column_index + 1, this_sheet.max_column + 1):
            temp_list.append(str(this_sheet.cell(row=i, column=j).value))
        
        result_list.append(temp_list)
    
    return result_list


def read_excel_to_numpy_2d_list(read_filename, start_row_index, start_column_index):
    '''
    读取Excel文件为2维numpy数组, 提供开始位置的行和列的索引(从0开始)
    注意：openpyxl中的Excel的单元格下标是从1开始的
    
    :param read_filename: 文件名
    :param start_row_index: 开始的行的索引
    :param start_column_index: 开始的列的索引
    '''
    
    work_book = load_workbook(read_filename)
    sheet_names = work_book.get_sheet_names()
    this_sheet = work_book.get_sheet_by_name(sheet_names[0])
    
    result_list = np.zeros((this_sheet.max_row - start_row_index, this_sheet.max_column - start_column_index))
    
    for i in range(start_row_index + 1, this_sheet.max_row + 1):
        for j in range(start_column_index + 1, this_sheet.max_column + 1):
            result_list[i - start_row_index - 1, j - start_column_index - 1] = this_sheet.cell(row=i, column=j).value
        
    return result_list
    

if __name__ == '__main__':
    now_directory = os.getcwd()
    root_directory = os.path.dirname(now_directory) + '/'
    #read_filename = root_directory + "dataset/read.txt"
    read_filename = root_directory + "dataset/read1.xlsx"
    delimiter = " "
    #result_list = read_to_1d_list(read_filename)
    #result_list = read_selected_column(read_filename, delimiter, 1)
    #result_list = read_to_2d_list(read_filename, delimiter)
    #result_list = read_to_2d_list_s(read_filename, delimiter, 2)
    #result_list = read_to_2d_list_se(read_filename, delimiter, 0, 2)
    #result_list = read_excel_to_2d_list(read_filename, 0, 0)
    result_list = read_excel_to_numpy_2d_list(read_filename, 0, 0)
    
    for each in result_list:
        #print(str(each, 'gbk'))
        print(each)
    
    #write_filename = root_directory + "dataset/write.txt"
    write_filename = root_directory + "dataset/write.xlsx"
    #quick_write_1d_to_text(write_filename, result_list)
    #write_1d_to_text(write_filename, result_list)
    write_to_excel(write_filename, result_list)
    