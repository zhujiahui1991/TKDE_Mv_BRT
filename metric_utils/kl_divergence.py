# -*- coding: utf-8 -*-
'''
Created on 2016年12月23日

@author: ZhuJiahui
'''

import numpy as np


def symmetric_kld(x1, x2):
    '''
    对称的KL Divergence
    :param x1: 分布1
    :param x2: 分布2
    '''
    x1 = x1 + 0.001
    x2 = x2 + 0.001
    x1 = x1 / np.sum(x1)
    x2 = x2 / np.sum(x2)
    
    kld1 = 0.0
    kld2 = 0.0
    for i in range(len(x1)):
        kld1 += x1[i] * np.log(x1[i] / x2[i])
        kld2 += x2[i] * np.log(x2[i] / x1[i])

    return np.abs((kld1 + kld2) / 2.0)


if __name__ == '__main__':
    pass