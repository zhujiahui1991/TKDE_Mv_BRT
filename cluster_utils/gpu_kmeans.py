# -*- coding: utf-8 -*-
'''
Created on 2017年4月24日

@author: ZhuJiahui506
'''

import os
import time
import numpy as np
from file_utils.file_reader import read_to_1d_list
from cluster_utils.kmeans_cluster import simple_means
from file_utils.file_writer import quick_write_1d_to_text

def assign_cluster():
    pass

def gpu_kmeans_test1():
    cluster_num = 15
    topic_num = 20
    now_directory = os.getcwd()
    root_directory = os.path.dirname(now_directory) + '/'
    read_directory1 = root_directory + 'dataset/gpu_lda/feed_topic' + str(topic_num)
    read_directory2 = root_directory + 'dataset/text_model/tm_corpus'
    
    write_directory = root_directory + 'dataset/gpu_lda_kmeans'
    write_filename1 = write_directory + '/cluster_inner_distance' + str(topic_num) + '_' + str(cluster_num) + '.txt'
    write_filename2 = write_directory + '/cluster_out_distance' + str(topic_num) + '_' + str(cluster_num) + '.txt'
    write_filename3 = write_directory + '/cluster_purity' + str(topic_num) + '_' + str(cluster_num) + '.txt'
    
    if (not(os.path.exists(write_directory))):
        os.mkdir(write_directory)
    
    inner = []
    outer = []
    purity = []
    
    for i in range(60, 70):
        start_time = time.clock()
        
        cluster_data = np.loadtxt(read_directory1 + '/' + str(i) + '.txt')
        class_tag = read_to_1d_list(read_directory2 + '/' + str(i) + '/' + str(i) + '.tag')
        
        cluster_inner_distance, cluster_out_distance, kmeans_purity = simple_means(cluster_num, cluster_data, class_tag)
        
        inner.append(str(cluster_inner_distance))
        outer.append(str(cluster_out_distance))
        purity.append(str(kmeans_purity))
        
        this_time = time.clock() - start_time
        print(this_time)
    quick_write_1d_to_text(write_filename1, inner)
    quick_write_1d_to_text(write_filename2, outer)
    quick_write_1d_to_text(write_filename3, purity)
    
    print('gpu_lda_kmeans complete')

if __name__ == '__main__':
    gpu_kmeans_test1()
    