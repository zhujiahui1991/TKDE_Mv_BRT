# -*- coding: utf-8 -*-
'''
Created on 2016年3月16日

@author: ZhuJiahui506
'''

import os
import numpy as np
import time
from file_utils.file_reader import read_to_2d_list

def pmi(topic_dis, w_corpus, marginal_dis):
    
    is_index = []
    for i in range(len(topic_dis)):
        if topic_dis[i] > 0.0001:
            is_index.append(i)

    temp_sum = 0.0
    for i in range(len(is_index)):
        for j in range(i, len(is_index)):
            if i == j:
                pass
            else:
                hit_count = 0
                for k in range(len(w_corpus)):
                    if (str(is_index[i]) in w_corpus[k]) and (str(is_index[j]) in w_corpus[k]):
                        hit_count += 1
                joint_p = np.true_divide(hit_count, len(w_corpus))
                
                if joint_p <=0.0001:
                    temp_sum += 0
                else:
                    temp_sum += np.log(joint_p / marginal_dis[is_index[i]] / marginal_dis[is_index[j]])

    pmi_score = np.true_divide(2 * temp_sum, len(is_index) * (len(is_index) - 1))
    
    return pmi_score

def pmi2(topic_dis, w_corpus, marginal_dis, domain_knowledge):
    
    is_index = []
    for i in range(len(topic_dis)):
        if topic_dis[i] > 0.0001:
            is_index.append(i)

    temp_sum = 0.0
    for i in range(len(is_index)):
        for j in range(i, len(is_index)):
            if i == j:
                pass
            else:
                hit_count = 0
                for k in range(len(w_corpus)):
                    each_doc_flag = 0
                    for each_left in domain_knowledge[is_index[i]]:
                        for each_right in domain_knowledge[is_index[j]]:
                            if (each_left in w_corpus[k]) and (each_right in w_corpus[k]):
                                hit_count += 1
                                each_doc_flag = 1
                                break
                        
                        if each_doc_flag == 1:
                            break
                joint_p = np.true_divide(hit_count, len(w_corpus))
                
                if joint_p <=0.0001:
                    temp_sum += 0
                else:
                    temp_sum += np.log(joint_p / marginal_dis[is_index[i]] / marginal_dis[is_index[j]])

    pmi_score = np.true_divide(2 * temp_sum, len(is_index) * (len(is_index) - 1))
    
    return pmi_score

def lda_pmi_test():
    start = time.clock()
    now_directory = os.getcwd()
    root_directory = os.path.dirname(now_directory) + '/'
    
    latent_topic_number = 20

    read_directory1 = root_directory + 'dataset/text_model/tm_corpus'
    read_directory2 = root_directory + 'dataset/lda/topic_word' + str(latent_topic_number)
    
    # 选取10片
    pmi_total = []
    for i in range(60, 70):
        
        w_corpus = read_to_2d_list(read_directory1 + '/' + str(i) + '/' + str(i) + '.docs', ' ')
        topic_word = np.loadtxt(read_directory2 + '/' + str(i) + '.txt')
        
        # 计算边缘概率
        marginal_dis = np.zeros(len(topic_word[0]))
    
        for k in range(len(w_corpus)):
            for j in range(len(w_corpus[k])):
                marginal_dis[int(w_corpus[k][j])] += 1.0
    
        marginal_dis = np.true_divide(marginal_dis, len(w_corpus))
        
        # 计算PMI值
        pmi_temp = []
        for j in range(len(topic_word)):
            pmi_score = pmi(topic_word[j], w_corpus, marginal_dis)
            pmi_temp.append(pmi_score)
        
        pmi_total.append(np.average(pmi_temp))
    
    print(np.average(pmi_total))
    
    print('Total time %f seconds' % (time.clock() - start))
    
def gpu_lda_pmi_test():
    start = time.clock()
    now_directory = os.getcwd()
    root_directory = os.path.dirname(now_directory) + '/'
    
    latent_topic_number = 20

    read_directory1 = root_directory + 'dataset/text_model/tm_corpus'
    read_directory2 = root_directory + 'dataset/gpu_lda/topic_word' + str(latent_topic_number)
    read_directory3 = root_directory + 'dataset/DK_set'
    
    # 选取10片
    pmi_total = []
    for i in range(60, 70):
        
        w_corpus = read_to_2d_list(read_directory1 + '/' + str(i) + '/' + str(i) + '.docs', ' ')
        topic_word = np.loadtxt(read_directory2 + '/' + str(i) + '.txt')
        domain_knowledge = read_to_2d_list(read_directory3 + '/' + str(i) + '.txt', ' ')
        
        # 计算边缘概率
        marginal_dis = np.zeros(len(topic_word[0]))
    
        for k in range(len(w_corpus)):
            for j in range(len(w_corpus[k])):
                marginal_dis[int(w_corpus[k][j])] += 1.0
    
        marginal_dis = np.true_divide(marginal_dis, len(w_corpus))
        
        # 计算PMI值
        pmi_temp = []
        for j in range(len(topic_word)):
            pmi_score = pmi2(topic_word[j], w_corpus, marginal_dis, domain_knowledge)
            pmi_temp.append(pmi_score)
        
        pmi_total.append(np.average(pmi_temp))
    
    print(np.average(pmi_total))
    
    print('Total time %f seconds' % (time.clock() - start))
    
if __name__ == '__main__':
    
    lda_pmi_test()
    # gpu_lda_pmi_test()
    
    
